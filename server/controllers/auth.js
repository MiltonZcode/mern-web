const jwt = require("../services/jwt");
const moment = require("moment");
const User = require("../models/user");


//! ---- Verificar validez de token en server para mas seguridad -----!!

function willExpiredToken(token){
  const {exp} = jwt.decodedToken(token); //* decodificar token
  const currentDate = moment().unix(); //* obtener hora actual en ms

  if(currentDate > exp){ //* comparar hora actual con expiracion de token
    return true; //* si caduco
  }
  return false; //* si es valido

};
//! ----- Fin verificar validez token en server ----------------------!!

//! ----- Refrescar tokens caducos ---------------------------!!
function refreshAccessToken(req, res) {
  const {refreshToken} = req.body;
  const isTokenExpired = willExpiredToken(refreshToken);

  if(isTokenExpired){
    res.status(404).send({message: "ERROR Token expired!!"})
  } else {
    const {id} = jwt.decodedToken(refreshToken);
    User.findOne({_id: id}, (err, userStored)=> {
      if(err){
        res.status(500).send({message: "Server ERROR!!"});
      } else {
        if (!userStored){
          res.status(404).send({message: "User not found!!!"});
        } else {
          res.status(200).send({
            accessToken: jwt.createAccessToken(userStored),
            refreshToken: refreshToken
          })
        }
      }
    })
  }
};

module.exports = {
  refreshAccessToken
};

//! ----- Fin refrescar tokens ------------------------------!!

