const bcrypt = require("bcrypt-nodejs");
const jwt = require("../services/jwt");
const User = require("../models/user");

function signUp(req, res){
  const user = new User(); //instancia un nuevo objeto user creado en models/user.js

  // console.log(req.body);

  const {name, lastname, email, password, repeatPassword} = req.body; //destructura array de datos que viene en parametros desde el body
  user.name = name;
  user.lastname = lastname;
  user.email = email.toLowerCase();
  user.role = "admin";
  user.active = false; //add user data to BD

  if (!password || !repeatPassword) { //si password o repeatPassword no estan en los params 
    res.status(404).send({message: "Password is required"}); //error y mensaje
  }else{
    if (password !== repeatPassword){//si el password es diferente
      res.status(404).send({message: "Password not match!!"}) //mensaje de error
    }else{
      //res.status(200).send({message: "Puede continuar..."}) //si coincide, mensaje success
      //!----------------------------------------------------------------------------------
      bcrypt.hash(password, null, null, function(err, hash){
        if(err) {
          res.status(500).send({message: err + "Error en encriptacion"});
        }else{
          //res.status(200).send({message: hash});//si no hay error devuelve el hash
          user.password = hash; //se envia hash

          user.save((err, userStored)=> { //guarda el nuevo usuario
            if(err) {
              res.status(500).send({message: "User already exist!!!!"});//error de email repetido
            }else{
              if(!userStored){
                res.status(404).send({message: err +  "error 404"});
              }else{
                res.status(200).send({user: userStored, message: "Success"});//el usuario se guardo
              }
            }
          });

        }
      });
    //bcrypt.nodejs encripta el password, uso bcrypt.hash(password, salt, null, cb(err,success))
    //En este ejemplo no usa salt    
      //!-----------------------------------------------------------------------------------
    };
  };
};

function signIn(req, res){
  const params = req.body;
  const email = params.email.toLowerCase();
  const password = params.password;

  User.findOne({email}, ( err, userStored )=> { //* Buscar en el Modelo User por email
    //* userStored = objeto con datos del user si existe
    if(err){       
      res.status(500).send({message: "Error 500 Server error!!!"});
    } else {
      if(!userStored){
        res.status(404).send({message: "Error 404 User not found!!!" });
      } else{  //* usamos bcrypt para comparar contraseña
        bcrypt.compare(password, userStored.password, (err, valid)=> {
          if(err){
            res.status(500).send({message: "Error 500-1 Server error!!!" });
          }else if(!valid) {
            res.status(404).send({message: "Invalid password!!!" });
          } else {
            if(!userStored.active){ //Usuario es valido pero no esta activo
              res.status(200).send({code: 200, message: "User doesn't active!!!"});
            } else {
              res.status(200).send({
                accessToken: jwt.createAccessToken(userStored), //*Crea el token enviando como parametro el user
                refreshToken: jwt.createRefreshToken(userStored) //* Estos parametros userStored se envian a las funciones createAccessToken y createRefreshToken en services/jwt...
              })
            }
          }
        })
      }
    }
  }) 
};

function getUsers(req, res){
  //Creando la ruta para obtener lista de usuarios en el crud. Se exporta el modulo y pasamos a crear el EndPoint en routers/user
  // console.log("Obteniendo lista de usuarios");

  User.find().then(users => {
    if(!users){
      res.status(404).send({message: "Users not found"});
    } else {
      res.status(200).send({users});
    }
  });
}
function getUsersActive(req, res){
  //Creando la ruta para obtener lista de usuarios activos.
  const query = req.query;

  User.find({active: query.active}).then(users => {
    if(!users){
      res.status(404).send({message: "Users not found"});
    } else {
      res.status(200).send({users});
    }
  });
}

module.exports = {
  signUp,
  signIn,
  getUsers,
  getUsersActive
};