const jwt = require("jwt-simple");
const moment = require("moment");

const SECRET_KEY = "r3782AGFgrfdeAFffas";

exports.ensureAuth = (req, res, next) => {
  //* Authorization es un parametro del header que transporta el token. La siguiente sentencia analiza si recibe un header authorization. Si se recibe, se hace replace del token (??)
  if (!req.headers.authorization){
    return res.status(403).send({message: "Autorizathion header not found"});
  }

  const token = req.headers.authorization.replace(/[' "]+/g, ""); //TODO buscar info de esto, palabras claves Bearer, replace, Authorization... Algo encontrado
  //TODO  https://www.developerro.com/2019/03/12/jwt-api-authentication/
  //TODO  https://www.oscarblancarteblog.com/2018/01/16/implementar-json-web-tokens-nodejs/

  try {
    var payload = jwt.decode(token, SECRET_KEY);

    if (payload.exp <= moment.unix()) {
      return res.status(404).send({message: "Token expired"});
    }
  } catch(ex){
    // console.log(ex);
    return res.status(404).send({message: "Invalid Token"});
  } 

  req.user = payload;
  next();
  
};