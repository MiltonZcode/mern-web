const mongoose = require("mongoose");
const app = require("./app");
const {API_VERSION, IP_SERVER, PORT_DB, PORT_SERVER, MONGO_VERSION} = require("./config");

/* ---------------- levantar servidor y conectar con MongoDB--------------------- */

mongoose.connect(`mongodb://${IP_SERVER}:${PORT_DB}/mernweb`, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false}, (err, res)=>{
    if (err){
        throw err;        
    }else{
        console.log (`mongoDB connect successfully // MongoDB version ${MONGO_VERSION}`);
        app.listen(PORT_SERVER, ()=>{
            console.log("=====================");
            console.log("===   API  REST   ===");
            console.log("=====================");
            console.log(`http://${IP_SERVER}:${PORT_SERVER}/api/${API_VERSION}/`);
        });
    };
}); 

/* -------------------------------------------------------------------------------- */