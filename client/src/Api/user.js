import {basePath, apiVersion} from "./config";

export function signUpApi(data){ //conectar con function(signUp) en user.js/controllers del server
  const url = `${basePath}/${apiVersion}/sign-up`;
  const params = {
    method: "POST",
    body: JSON.stringify(data), //convierte los params de 'data' en formato JSON
    headers: {
      "Content-Type": "application/json"
    }
  };  
//Este return fetch se conecta al servidor
  return fetch(url, params).then(response =>{
    return response.json();
  }).then(result => {    
    if(result.user){
      return {Ok: true, message: "Operation success"};
    }
    return {Ok: false, message: result.message};
  }).catch (err => {
    return {Ok: false, message: err.message};
  })
};


export function signInApi(data){
  const url = `${basePath}/${apiVersion}/sign-in`;
  const params = {
    method: "POST",
    body: JSON.stringify(data),
    headers:{
      "Content-Type": "application/json"
    }
  };

  return fetch(url, params)
    .then(response => {      
      return response.json();
    })
    .then(result => {
      console.log(result);
      return result;  
    })
    .catch(err =>{
      return err.message;
    });
    
};

export function getUsersApi(token){
  const url = `${basePath}/${apiVersion}/users`;
  const params = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: token
    }
  };

  return fetch(url, params)
    .then(response => {
      return response.json();
    })
    .then(result => {
      return result;
    })
    .catch(err =>{
      return err.message;
    });


};

export function getUsersActiveApi(token, status){

  const url = `${basePath}/${apiVersion}/users-active?active=${status}`;
  
  const params = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: token
    }
  };

  return fetch(url, params)
    .then(response => {
      return response.json();
    })
    .then(result => {
      return result;
    })
    .catch(err =>{
      return err.message;
    });


};