import {basePath, apiVersion} from "./config";
import {ACCESS_TOKEN, REFRESH_TOKEN} from "../utils/constants";
import jwtDecode from "jwt-decode";

//!------ Obtener tokens y comprobar validez, OK si son validos ----!!

export function getAccessTokenApi(){
  const accessToken = localStorage.getItem(ACCESS_TOKEN); //Obtiene token almacenado
  
  if(!accessToken || accessToken === "null"){    
    return null;
  }  
  return willExpireToken(accessToken) ? null : accessToken;//llama a la funcion willExpireToken
};

export function getRefreshTokenApi(){
  const refreshToken = localStorage.getItem(REFRESH_TOKEN); //Obtiene token almacenado

  if(!refreshToken || refreshToken === "null"){
    return null;
  }

  return willExpireToken(refreshToken) ? null : refreshToken;//llama a la funcion willExpireToken
};

export function refreshAccessTokenApi(refreshToken){
  const url = `${basePath}/${apiVersion}/refresh-access-token`;
  const bodyObj = {
    refreshToken: refreshToken
  }
  const params = {
    method: "POST",
    body: JSON.stringify(bodyObj),
    headers: {
      "Content-Type": "application/json"
    }
  };

  fetch (url, params)
  .then(response => {
    if (response.status !== 200) {
      return null;
    }
    return response.json();
  })
  .then(result => {
    if(!result){
      logout();
    }else {
      const {accessToken, refreshToken} = result;
      localStorage.setItem(ACCESS_TOKEN, accessToken);
      localStorage.setItem(REFRESH_TOKEN, refreshToken);  
    }

  });

};

export function logout(){
  localStorage.removeItem(ACCESS_TOKEN);
  localStorage.removeItem(REFRESH_TOKEN);
}





function willExpireToken(token){ //Comprueba la validez del token
  const seconds = 60;
  const metaToken = jwtDecode(token); //decodifica el token
  const {exp} = metaToken; //destructura la fecha de expiracion en ms
  const now = (Date.now() + seconds)/ 1000; //Obtener hora actual en ms

  return now > exp; //true si ahora es mayor que exp, false si es menor o =
 
};

//! -------- Fin Obtener token valido ---------------------------------!!

