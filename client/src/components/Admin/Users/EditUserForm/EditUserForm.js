import React, {useState, useCallback} from "react";
import {Avatar, Form, Input, Select, Button, Row, Col} from "antd";
import {UserOutlined, MailOutlined} from "@ant-design/icons";
import {useDropzone} from "react-dropzone";
import NoAvatar from "../../../../assets/img/png/no-avatar.png"


import "./EditUserForm.scss";


export default function EditUserForm(props){  
  const {user} = props;
  const [avatar, setAvatar] = useState(null);
  const [userData, setUserData] = useState({
    name: user.name,
    lastname: user.lastname,
    email: user.email,
    role: user.role,
    avatar: user.avatar
  });

  

    const updateUser = evt => {      
      console.log("********"); 
    };

  return (
    <div className="edit-user-form">
      <UploadAvatar avatar={avatar} setAvatar={setAvatar}/>
      <EditForm user={user} userData={userData} setUserData={setUserData} updateUser={updateUser}/>
    </div>
  )
}

function UploadAvatar(props){
  const {avatar, setAvatar} = props;

  const onDrop = useCallback(
    acceptedFiles => {
      const file = acceptedFiles[0];
      setAvatar({file, preview: URL.createObjectURL(file)})

    }, [setAvatar]
  );

  const {getRootProps,getInputProps, isDragActive} = useDropzone({
    accept: "image/jpeg, image/png",
    noKeyboard: true,
    onDrop
  });

  return (
    <div className="upload-avatar" {...getRootProps()}>
      <input {...getInputProps()}/>
      {isDragActive ? (<Avatar size={150} src={NoAvatar} />
      ) : (<Avatar size={150} src={avatar ? avatar.preview : NoAvatar} />)}
    </div>
  );

};

function EditForm(props){
  const {user, userData, setUserData, updateUser} = props;
  const {Option} = Select; //* antd component

  console.log(`DATA QUE LLEGA EN PROPS ${userData.name}${userData.lastnamename}${userData.email}`)
  console.log(`DATA QUE LLEGA EN PROPS ${user.name}${user.lastnamename}${user.email} ${user.role}`)

  return (    
     <Form className="form-edit" onFinish={updateUser}>
      <Row gutter={24 }>
        <Col span={12}>
        <Form.Item>
            <Input 
              prefix={<UserOutlined />}
              placeholder="Nombre"
              defaultValue={user.name}
              onChange={evt => setUserData({...userData, name: evt.target.value})}
            />
          </Form.Item>
        </Col>

        <Col span={12}>
          <Form.Item>
            <Input 
              prefix={<UserOutlined />}
              placeholder="Apellido"
              defaultValue={user.lastname}
              onChange={evt => setUserData({...userData, lastname: evt.target.value})}
            />
          </Form.Item>
        </Col>

      </Row>

      <Row gutter={24 }>
        <Col span={12}>
          <Form.Item>
            <Input 
              prefix={<MailOutlined />}
              placeholder="user@email"
              value={user.email}
              onChange={evt => setUserData({...userData, email: evt.target.value})}
            />
          </Form.Item>
        </Col>

        <Col span={12}>
          <Select
            placeholder="Select a Role..."
            onChange={evt => setUserData({...userData, role: evt})}
            defaultValue={user.role}
          >
            <Option value="admin">Administrador</Option>
            <Option value="editor">Editor</Option>
            <Option value="reviewer">Revisor</Option>
          </Select>
        </Col>
      </Row>

      <Row gutter={24 }>
        <Col span={12}>
          
        </Col>

        <Col span={12}>
          
        </Col>
      </Row>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="btn-submit">
          Actualizar Usuario
        </Button>
      </Form.Item>
     </Form>
  )

}