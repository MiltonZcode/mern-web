import React, {useState} from "react";
import {Form, Input, Button, Checkbox, notification} from "antd";
import {UserAddOutlined, UnlockOutlined} from "@ant-design/icons";
import {emailValidation, minLengthValidation} from "../../../utils/formValidation";
import {signUpApi} from "../../../Api/user";

import "./RegisterForm.scss";



export default function RegisterForm() {
  //difinicion de estado del form. Se importa useState de React.
  //const [valor, estadoValor] = useState({objeto a actualizar})
  const [inputs, setInputs] = useState({ 
    email:"",
    password:"",
    repeatPassword:"",
    privacyPolicy: false
    //Estado por default del formulario (valores vacios)
    //las variables deben tener el mismo nombre que el atributo 'name' del input
  });

  const [formValid, setFormValid] = useState({
    email: false, 
    password: false,
    repeatPassword: false,
    privacyPolicy: false
    //las variables deben tener el mismo nombre que el atributo 'name' del input
  });

  //Actualizar valores del Form
  const changeForm = evt => {
    if(evt.target.name === "privacyPolicy"){
      setInputs({
        ...inputs,
        [evt.target.name]: evt.target.checked 
      });
    }else{
        setInputs({
          ...inputs,
          [evt.target.name]: evt.target.value          
        });
      }            
  };

  const inputValidation = evt => {
    const {type, name} = evt.target;

    if (type === "email") {
      setFormValid({
        ...formValid, //trae los valores de constante useState
        [name]: emailValidation(evt.target) //toma name del input en foco y valida
      });
    };

    if (type === "password") {
      setFormValid({
        ...formValid, 
        [name]: minLengthValidation(evt.target, 4) 
      });
    };

    if (type === "checkbox") {
      setFormValid({
        ...formValid, 
        [name]: evt.target.checked
      });
    };
  };
  

  const register = async evt => {
    // const {email, password, repeatPassword, privacyPolicy} = formValid;
    const emailVal = inputs.email; 
    const passwordVal = inputs.password;
    const repPassVal = inputs.repeatPassword;
    const privPolicy = inputs.privacyPolicy;

    if(!emailVal || !passwordVal || !repPassVal || !privPolicy){
      notification["error"]({
        message: "All fields is required"
      });
    }else {
      if(passwordVal !== repPassVal){
        notification["error"]({
          message: "Password don't match"
        });
      }else {
        const result = await signUpApi(inputs);
          if(!result.Ok){
            notification["error"]({message: result.message})
          } else{
            notification["success"]({message: result.message});
            resetForm();
          }      
      }
    }
  };

  const resetForm = () => {
    const inputs = document.getElementsByTagName("input");

    for (let i = 0; i < inputs.length; i++){
      inputs[i].classList.remove("success");
      inputs[i].classList.remove("error");
    }

    setInputs({
      email:"",
      password:"",
      repeatPassword:"",
      privacyPolicy: false
    });

    setFormValid({
      email:false,
      password:false,
      repeatPassword:false,
      privacyPolicy: false
    });

  };

  return (
    <Form className = "register-form" onFinish = {register} onChange = {changeForm}>
      
        <Form.Item>
          <Input
            prefix = {<UserAddOutlined style={{color: "rgba(0,0,0,.25)"}}/>} 
            type = "email"
            name = "email"
            placeholder = "Enter Email"
            className = "register-form__input"
            onChange = {inputValidation} //llama funcion inputValidation (validar datos)
            value = {inputs.email} //pasa el estado al hook useState
          />
        </Form.Item>
        <Form.Item>
          <Input
            prefix = {<UnlockOutlined style={{color: "rgba(0,0,0,.25)"}}/>}
            type = "password"
            name = "password"
            placeholder = "Enter password"
            className = "register-form__input"
            onChange = {inputValidation} //llama funcion inputValidation (validar datos)
            value = {inputs.password}
          />
        </Form.Item>
        <Form.Item>
          <Input
            prefix = {<UnlockOutlined style={{color: "rgba(0,0,0,.25)"}}/>}
            type = "password"
            name = "repeatPassword"
            placeholder = "Reenter password"
            className = "register-form__input" 
            onChange = {inputValidation} //llama funcion inputValidation (validar datos)
            value = {inputs.repeatPassword}
          />
        </Form.Item>
        <Form.Item>
          <Checkbox 
            name = "privacyPolicy" 
            onChange = {inputValidation} 
            checked = {inputs.privacyPolicy}>
            He leido, comprendo y acepto las politicas de privacidad
          </Checkbox>
        </Form.Item>
        <Form.Item>
          <Button htmlType = "submit" className = "register-form__button">
            Crear Cuenta
          </Button>
        </Form.Item>
      
    </Form>
  );
}