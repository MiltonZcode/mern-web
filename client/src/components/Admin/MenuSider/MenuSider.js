import React from "react";
import {Link, withRouter} from "react-router-dom";
import {Layout, Menu} from "antd";
import {HomeOutlined, MenuUnfoldOutlined, UserSwitchOutlined} from "@ant-design/icons";



import "./MenuSider.scss";

 function Menusider(props) {
    const {menuCollapsed, location} = props;
    const {Sider} = Layout;

    // console.log(location.pathname);

  return (
    <Sider className="admin-sider" collapsed={menuCollapsed}>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={[location.pathname]}>
        <Menu.Item key="/admin">
          <Link to={"/admin"}>
            <HomeOutlined />
            <span className="nav-text">Home</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/users">
          <Link to={"/admin/users"}>
            <UserSwitchOutlined />
            <span className="nav-text">User page</span>
          </Link>
        </Menu.Item>      
      </Menu>
    </Sider>
  );
}

export default withRouter(Menusider); //* withRoute de React devuelve la ruta actual. En este caso es util para decirle al menu en que ruta estamos y pintar el icono correcto en Users page