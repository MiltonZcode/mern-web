import React from "react";
import {Button} from "antd";
import {MenuFoldOutlined, MenuUnfoldOutlined, PoweroffOutlined} from "@ant-design/icons";
import menuLogo from "../../../assets/img/svg/logo.svg";
import {logout} from "../../../Api/auth"


import "./MenuTop.scss";

export default function MenuTop(props) {
  const {menuCollapsed, setMenuCollapsed} = props;

  const logoutUser = ()=> {
    logout();
    window.location.reload();
  };

  return (
    <div className="menu-top">
      <div className="menu-top_left">
        <img className="menu-top_logo" src={menuLogo} alt="logo" />
        <Button type="link" onClick={()=> setMenuCollapsed(!menuCollapsed)} >          
           {menuCollapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        </Button>
      </div>
      <div className="menu-top_right">
        <Button type="link" onClick={logoutUser}>        
          <PoweroffOutlined />
        </Button>
      </div>
    </div>
  )
}