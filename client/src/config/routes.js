//Layout
import LayoutAdmin from "../layouts/layoutAdmin";
import LayoutBasic from "../layouts/layoutBasic";

// Admin pages

import adminHome from "../pages/Admin"; //traigo modulo desde /pages/Admin/Admin.js
import adminSignIn from "../pages/Admin/SignIn"; //traigo modulo desde /pages/Admin/SigIn.js
import adminUsers from "../pages/Admin/Users"; //traigo modulo desde /pages/Admin/Users

// basic pages

import Home from "../pages/Home";
import Contact from "../pages/Contact";

//Other pages

import Error404 from "../pages/error404";

const routes = [
  {
    path: "/admin",
    component: LayoutAdmin,
    exact: false,
    routes: [
      {
        path: "/admin",
        component: adminHome,
        exact: true
      },
      {
        path: "/admin/login",
        component: adminSignIn,
        exact: true
      },
      {
        path: "/admin/users",
        component: adminUsers,
        exact: true
      },
      {
        component: Error404
      }
    ]
  },
  {
    path: "/",
    component: LayoutBasic,
    exact: false,
    routes:[
      {
        path: "/",
        component: Home,
        exact: true
      },
      {
        path: "/contact",
        component: Contact,
        exact: true
      },
      {
        component: Error404
      }
    ] 
  }
]

export default routes;


