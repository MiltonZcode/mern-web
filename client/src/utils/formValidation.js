export function minLengthValidation(inputData, minLength){
 const {value} = inputData; //se destructura el 'value' del input

 removeClassErrorSuccess(inputData);//limpia las clases 'success' o 'error' si existen

 if(value.length >= minLength) {
   inputData.classList.add("succes"); //Si el value tiene los caracteres minimos reuqeridos
   return true;   
 } else{
   inputData.classList.add("error");
   return false;
 }
};

export function emailValidation(inputData){
  // eslint-disable-next-line no-useless-escape
  const emailValid = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  const {value} = inputData; //se destructura el 'value' del input

  removeClassErrorSuccess(inputData);//limpia las clases 'success' o 'error' si existen

  const resultValidation = emailValid.test(value);//Aplico metodo test() para validar
  if(resultValidation) {
      inputData.classList.add("success"); //Si el value tiene los caracteres minimos reuqeridos
    return true;   
  } else{
    inputData.classList.add("error");
    return false;
  }
};

function removeClassErrorSuccess(inputData){
  inputData.classList.remove('success');
  inputData.classList.remove('error');
};