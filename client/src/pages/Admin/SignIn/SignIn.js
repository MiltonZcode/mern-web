import React from 'react';
import {Layout, Tabs} from "antd";
import {Redirect} from "react-router-dom";
import Logo from "../../../assets/img/svg/logo.svg";
import RegisterForm from "../../../components/Admin/RegisterForm";
import LoginForm from "../../../components/Admin/LoginForm";
import {getAccessTokenApi} from "../../../Api/auth";

import "./SignIn.scss";



export default function SignIn() {
  const {Content} = Layout;  
  const {TabPane} = Tabs;
  
  if (getAccessTokenApi()){
    return <Redirect to="/admin" />;
  } //* Si esta logueado bloquea acceso a login redirigiendo 

    return (
        <Layout className= "sign-in">
          <Content className= "sign-in__content">
            <h1 className="sign-in__content-logo">
              <img src={Logo} alt="Logo"/>
            </h1>
            <div className="sign-in__content-tabs">
              <Tabs type= "card">
                <TabPane tab={<span>Entrar</span>} key="1">
                  <LoginForm />
                </TabPane>
                <TabPane tab={<span>Nuevo User</span>} key="2">
                  <RegisterForm />
                </TabPane>
              </Tabs>
            </div>
          </Content>
        </Layout>
    );
};

//traigo Content del component Layout de antd
 //traigo compo TabPanel de Tabs antd