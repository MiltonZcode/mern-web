/* creacion de layouts que se repetiran en diferentes paginas de la web */
import React, {useState} from 'react';
import {Redirect, Route, Router, Switch} from 'react-router-dom';
import {Layout} from 'antd';
import useAuth from "../hooks/useAuth";
import MenuTop from "../components/Admin/MenuTop";
import MenuSider from "../components/Admin/MenuSider";
import AdminSignIn from "../pages/Admin/SignIn"; //page singin


// import {getAccessToken, getRefreshToken} from "../Api/auth"; //? Prueba Tokens

import "./layoutAdmin.scss";



export default function LayoutAdmin(props){

  const {routes} = props;  
  const [menuCollapsed, setMenuCollapsed] = useState(false);
  const {Header, Content, Footer} = Layout;
  const {user, isLoading} = useAuth(); //isLoading true mientras carga

  //TODO BORRAR *******************************

  console.log(user);
  console.log(isLoading);

  //TODO BORRAR *******************************
  

  if(!user && !isLoading){ //*Si termino de cargar y user is null
    return (
      <>
       <Route path="/admin/login" component={AdminSignIn} /> 
       <Redirect to="/admin/login" />
      </>
    )
  };


  if(user && !isLoading) { //* user logueado, pasa a admin page    
    return (
      <Layout> 
          <MenuSider menuCollapsed={menuCollapsed} />
			  <Layout className="layout-admin" style={{marginLeft: menuCollapsed ? "80px" : "200px"}}>
				  <Header className="layout-admin_header">
            <MenuTop menuCollapsed={menuCollapsed} setMenuCollapsed={setMenuCollapsed}/>
          </Header>
          <Content className="layout-admin_content">
            <LoadRoutes routes={routes}/>
          </Content>
          <Footer className="layout-admin_footer">Diseño MERN 2021</Footer>
			  </Layout>				
      </Layout> 
    )       
  }; 

  return null; 
}

function LoadRoutes({routes}){
  return(
    <Switch> 
      {routes.map((route, index)=> (    
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />    
      ))};
    </Switch>
  );
};