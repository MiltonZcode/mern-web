/* creacion de layouts que se repetiran en diferentes paginas de la web */
import React from 'react';
import {Route, Switch} from "react-router-dom"
import {Layout} from 'antd';

import "./layoutBasic.scss";

export default function LayoutBasic(props){

  const {routes} = props;
  const {Content, Footer} = Layout;

  return(    
      <Layout> 
        <h2>...</h2>				
				<Layout>
          <Content>
            <LoadRoutes routes={routes}/>
          </Content>
          <Footer> Copiar Footer </Footer>
        </Layout>
      </Layout>     
  );
};

function LoadRoutes({routes}){
  return(
    <Switch> 
      {routes.map((route, index)=> (    
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />    
      ))};
    </Switch>
  );
};